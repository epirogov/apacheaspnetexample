﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MySqlConnector
/// </summary>
public class MySqlConnector
{
    public MySqlConnector()
    {
        //mysql -uroot -p12345
        //show databases;
        //use mysql;
        //show tables; time_zone
        //DESCRIBE time_zone
        string connectionString =
          "Server=/var/lib/mysql/mysql.sock;Protocol=unix;" +
          "Database=mysql;" +
          "Uid=root;" +
          "Pwd=12345;";
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);
        dbcon.Open();
        IDbCommand dbcmd = dbcon.CreateCommand();
        // requires a table to be created named employee
        // with columns firstname and lastname
        // such as,
        //        CREATE TABLE employee (
        //           firstname varchar(32),
        //           lastname varchar(32));
        string sql =
            "SELECT `Host`, `User`" +
            "FROM `user`";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            string name = reader["Host"].ToString();
            string countryCode = reader["User"].ToString();
            Console.WriteLine("Name: " +
                  name + " " + countryCode);
        }
        // clean up
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        //dbcon.Close();
        dbcon = null;
    }
}