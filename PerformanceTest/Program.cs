﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentResponce = string.Empty, prevResponce = string.Empty;

            int i = 0;
            do
            {
                var client = new HttpClient();

                // Create the HttpContent for the form to be posted.
                var requestContent = new FormUrlEncodedContent(new[] {
    new KeyValuePair<string, string>("text", "This is a block of text"),
});

                // Get the response.
                HttpResponseMessage response = client.PostAsync(
                    "http://localhost",
                    null).Result;

                // Get the response content.
                HttpContent responseContent = response.Content;

                // Get the stream of the content.
                using (var reader = new StreamReader(responseContent.ReadAsStreamAsync().Result))
                {
                    // Write the output.
                    prevResponce = currentResponce;
                    currentResponce = reader.ReadToEnd();
                }
            }
            while (i++ == 0 || currentResponce != prevResponce || i < 1000);
            if(currentResponce != prevResponce)
            {
                Console.WriteLine(currentResponce);
            }
            else
            {
                Console.WriteLine("Success:"+i);
            }
        }
    }
}
